import HeadTable from "@/components/UI/HeadTable";
import DialogInput from "@/components/UI/DialogInput";
import InputTable from "@/components/UI/InputTable";
import ItemsTable from "@/components/UI/ItemsTable";
import ElementInfo from "@/components/UI/ElementInfo";
import PagesNumber from "@/components/UI/PagesNumber";



export default [
    HeadTable,
    DialogInput,
    InputTable,
    ItemsTable,
    ElementInfo,
    PagesNumber
]