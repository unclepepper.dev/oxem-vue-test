import { createApp } from 'vue'
import App from './App'
import router from "@/router";
import components from '@/components/UI';
import store from "@/store";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap";

const app = createApp(App)

components.forEach(component => {
    app.component(component.name, component)
})
app
    .use(router)
    .use(store)
    .mount('#app')